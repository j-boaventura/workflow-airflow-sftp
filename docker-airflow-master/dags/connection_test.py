"""
Code that goes along with the Airflow located at:
http://airflow.readthedocs.org/en/latest/tutorial.html
"""
from airflow import DAG
from datetime import datetime, timedelta
from airflow.operators.python_operator import PythonOperator
from connect_sftp import move_file



default_args = {
    'owner': 'Jordan',
    'start_date': datetime(2020, 2, 13),
    'retries': 1,
    'retry_delay': timedelta(seconds=5)
}

with DAG('move_file', default_args=default_args, schedule_interval='@daily', catchup=True) as dag:
    t1 = PythonOperator(task_id='move_file', python_callable=move_file)

